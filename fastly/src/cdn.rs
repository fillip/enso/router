use crate::s3;

use fastly::{Error, Request, Response};

/// Route handles requests to the content delivery network services.
/// At this stage we're only using one store ("cdn.fillip.pro hosted by Wasabi").
///
/// Later we will add private CDN access (with added, non-S3, authentication), and potentially
/// multiple buckets to handle various projects.
pub(crate) fn route(request: Request) -> Result<Response, Error> {
	let path_url = request.get_path();
	let new_request = Request::get(format!(
		"{}{}",
		"https://s3.eu-central-2.wasabisys.com/cdn.fillip.pro/fillip.pro", path_url
	));

	s3::route(new_request)
}
