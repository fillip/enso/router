use std::env;

use crate::RouteHandler;

use chrono::Utc;
use fastly::{http::StatusCode, Error, Request, Response};
use routefinder::Captures;
use serde::{Deserialize, Serialize};

// Define the backend for service
const HOME_FILLIP_PRO_BACKEND: &str = "home_fillip_pro";

/// Enum representing service routes (non-web application routes).
pub(crate) enum ServiceRoutes {
	Status,
}

impl ServiceRoutes {
	/// Creates a router for the service routes.
	///
	/// Registers the `"/status"` path.
	pub fn router() -> routefinder::Router<Self> {
		let mut router = routefinder::Router::<Self>::new();
		router.add("/status", Self::Status).unwrap();
		router
	}
}

/// Implements the `RouteHandler` trait for `ServiceRoutes`.
///
/// This defines the behavior for the `"/status"` route when called with a request.
impl RouteHandler for ServiceRoutes {
	fn exec(&self, request: Request, _captures: &Captures) -> Result<Response, Error> {
		match self {
			// Forward the request to the Deno backend for the `"/deno"` route
			Self::Status => route_status(&request),
		}
	}
}

/// Handles the `/status` route by checking the health of the backend.
///
/// This function performs a health check on the `home_eolian_group` backend
/// and returns the status of the backend along with the current timestamp.
///
/// # Returns
///
/// A `Response` object with the backend health status and a timestamp.
///
fn route_status(_request: &Request) -> Result<Response, Error> {
	
    // Define the structure for the health status response
	#[derive(Serialize, Deserialize)]
	struct OriginStatus {
		timestamp: String,
		fastly_pop: String,
		home_minttu_dev_health: bool,
	}

	// Get the Fastly POP (Point of Presence) environment variable
	let fastly_pop = env::var("FASTLY_POP").unwrap_or_else(|_| "POP_UNKNOWN".to_string());

	// Perform a health check by sending a request to the backend
	let home_fillip_pro_health = check_backend_health(HOME_FILLIP_PRO_BACKEND);

	// Construct the response JSON with the health status and current timestamp
	let status_json = serde_json::json!({
		"timestamp": Utc::now().format("%a, %d %h %Y %H:%M:%S UTC").to_string(),
		"fastly_pop": fastly_pop,
		"home_fillip_pro_health": home_fillip_pro_health,
	});

	// Return the response with a 200 OK status
	Ok(Response::from_body(status_json.to_string()).with_status(StatusCode::OK))
}

/// Performs a health check by sending a request to the specified backend.
///
/// This function sends a request to the backend to check if it responds with a
/// successful status.
///
/// # Arguments
/// * `backend_name` - The name of the backend to check.
///
/// # Returns
/// `true` if the backend responds with a successful status, `false` otherwise.
///
fn check_backend_health(backend_name: &str) -> bool {
	// Create a test request to send to the backend
	let request = Request::get("https://home-fillip-pro.deno.dev/"); 

	// Send the request to the backend and check if the response is successful
	match request.send(backend_name) {
		Ok(response) => response.get_status().is_success(),
		Err(_) => false, // If there's an error, assume the backend is unhealthy
	}
}
