use std::str;

use fastly::{secret_store::LookupError, SecretStore};

/// Configuration for Interoperability API
pub struct Credentials {
	pub access_key_id: String,
	pub secret_access_key: String,
	pub region: String,
	pub host: String,
}

impl Credentials {
	/// Load the S3 configuration.
	///
	/// This assumes an secret store named "`s3_config`" is attached to this service,
	/// with entries for `access_key_id`, `secret_key`, `region` and `bucket`.
	pub fn load() -> Self {
		let store = SecretStore::open("secrets_fillip_pro").unwrap();

		Self {
			access_key_id: get_secret_as_string(&store, "access_key_id"),
			secret_access_key: get_secret_as_string(&store, "secret_access_key"),
			region: get_secret_as_string(&store, "region"),
			host: get_secret_as_string(&store, "host"),
		}
	}
}

/// Gets a secret from a given SecretStore, with a given key.
fn get_secret_as_string(store: &SecretStore, key: &str) -> String {
	let value = String::from_utf8(
		store
			.get(key)
			.ok_or_else(|| LookupError::InvalidSecretName(key.to_string()))
			.expect("key not found")
			.plaintext()
			.to_vec(),
	)
	.expect("key not found in secret store");

	return value;
}
