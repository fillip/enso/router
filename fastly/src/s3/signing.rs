use hmac_sha256::{Hash, HMAC};

/// Generate authoriation header for S3 object access
/// <https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html>
#[allow(clippy::too_many_arguments)]
pub fn generate_authorization_header(
	access_key_id: &str,
	secret_access_key: &str,
	region: &str,
	host: &str,
	canonical_uri: &str,
	canonical_querystring: &str,
	x_amz_content_sha256: &str,
	method: &str,
	amz_date: &str,
	service: &str,
) -> String {
	let canonical_headers = format!(
		"host:{host}\n\
        x-amz-content-sha256:{x_amz_content_sha256}\n\
        x-amz-date:{amz_date}\n"
	);

	let signed_headers = "host;x-amz-content-sha256;x-amz-date";

	let canonical_request = format!(
		"{method}\n{canonical_uri}\n{canonical_querystring}\n{canonical_headers}\n{signed_headers}\n{x_amz_content_sha256}"
	);

	// credential_date format is YYYYMMDD
	let credential_date = &amz_date[..8];
	let credential_scope = format!("{credential_date}/{region}/{service}/aws4_request");

	// Compose string to be signed
	let canonical_request_hash = hex::encode(Hash::hash(canonical_request.as_bytes()));
	let string_to_sign =
		format!("AWS4-HMAC-SHA256\n{amz_date}\n{credential_scope}\n{canonical_request_hash}");

	// Generate signing key
	let aws4_secret = format!("AWS4{secret_access_key}").into_bytes();
	let date_key = sign(&aws4_secret, credential_date);
	let region_key = sign(&date_key, region);
	let service_key = sign(&region_key, service);
	let signing_key = sign(&service_key, "aws4_request");

	// Generate signature
	let signature = hex::encode(sign(&signing_key, &string_to_sign));

	// Compose authorization header value
	format!(
		"AWS4-HMAC-SHA256 Credential={access_key_id}/{credential_scope},SignedHeaders={signed_headers},Signature={signature}"
	)
}

/// Generate HMAC hash of message with key
fn sign(key: &[u8], message: &str) -> [u8; 32] {
	HMAC::mac(message.as_bytes(), key)
}
