use fastly::{http::StatusCode, Error, Request, Response};
use routefinder::Captures;

/// Trait to define how each route should handle requests.
///
/// Each route implements this trait to handle specific logic when a request
/// is made to that route.
pub(crate) trait RouteHandler {
	/// Executes the route-specific logic for the request.
	///
	/// - `request`: The incoming request object.
	/// - `captures`: The path parameters or captures, if any.
	fn exec(&self, request: Request, captures: &Captures) -> Result<Response, Error>;
}

/// Routes the request to the appropriate handler based on the path.
///
/// This function uses the router to find the best match for the incoming request
/// path and delegates the request to the appropriate handler.
pub(crate) fn handle_request<H: RouteHandler>(
	router: &routefinder::Router<H>,
	path: &str,
	request: Request,
) -> Result<Response, Error> {
	// Try to find the best match for the path
	router.best_match(path).map_or_else(
		|| {
			// If no match is found, return a 404 Not Found response
			Ok(
				Response::from_body("The page you requested could not be found")
					.with_status(StatusCode::NOT_FOUND),
			)
		},
		|matched_route| {
			// If a match is found, delegate to the appropriate handler
			let handler = matched_route.handler();
			handler.exec(request, &matched_route.captures())
		},
	)
}
