use fastly::{
	experimental::{BackendExt, BackendHealth},
	http::StatusCode,
	Backend, Error, Request, Response,
};

const BACKEND_ORIGIN: &str = "home_fillip_pro";

/// Route requests to the home app.
pub(crate) fn route(request: Request) -> Result<Response, Error> {
	// Send request to backend
	let backend = Backend::from_name(BACKEND_ORIGIN).map_err(|err| {
		println!("home not implemented");
		// TODO: custom backend failure response
		err
	})?;

	if !backend.exists() {
		let response = Response::from_body(format!("backend '{backend}' does not exist"))
			.with_status(StatusCode::NOT_FOUND);

		return Ok(response);
	}

	let backend_response = if backend.is_healthy()? != BackendHealth::Unhealthy {
		request.send(BACKEND_ORIGIN)?
	} else {
		println!("backend is not healthy");

		Response::from_body("The page you requested could not be found".to_string())
			.with_status(StatusCode::NOT_FOUND)
	};

	Ok(backend_response)
}
