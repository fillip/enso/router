use super::home;
use crate::routes::RouteHandler;

use fastly::{Error, Request, Response};
use routefinder::Captures;

/// Route (web) application requests to the appropriate backend service. This should be the typical
/// handler for any request that does not include the `Accept: application/json` header key/value.
///
/// For all `Accept: application/json` requests the assumption is made that they are web service requests
/// rather than content requests.
pub(crate) enum AppsRoutes {
	Home,
}

impl AppsRoutes {
	pub fn router() -> routefinder::Router<Self> {
		let mut router = routefinder::Router::<Self>::new();
		router.add("*", Self::Home).unwrap();
		router
	}
}

impl RouteHandler for AppsRoutes {
	fn exec(&self, request: Request, _captures: &Captures) -> Result<Response, Error> {
		match self {
			Self::Home => home::route(request),
		}
	}
}
