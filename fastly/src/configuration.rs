use std::str::FromStr;

use fastly::ConfigStore;
use log::LevelFilter;

const CONFIG_STORE_NAME: &str = "config_fillip_pro";

pub(crate) fn get_log_level() -> LevelFilter {
	let config_store =
		ConfigStore::try_open(CONFIG_STORE_NAME).expect("config store for fillip.pro unavailable");

	let log_level_raw = config_store
		.try_get("log_level")
		.expect("log_level not set in config store")
		.unwrap_or_else(|| LevelFilter::Info.to_string());

	LevelFilter::from_str(&log_level_raw).unwrap_or(LevelFilter::Debug)
}
