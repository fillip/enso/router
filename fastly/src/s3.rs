mod config;
mod signing;

use crate::s3::config::Credentials;
use crate::s3::signing::generate_authorization_header;

use fastly::http::{header, Method, StatusCode};
use fastly::{Error, Request, Response};

/// S3 Backend Name
/// The backend domain name can be set as `s3.BUCKET_REGION.amazonaws.com`
/// For other options check <https://docs.aws.amazon.com/general/latest/gr/s3.html>
const BACKEND: &str = "s3_content_host";

/// Hash of empty string, used for authentication string generation
/// caculated from hex::encode(Hash::hash("".as_bytes()));
const EMPTY_HASH: &str = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

/// Route S3 content requests to the configured S3 host.
pub(crate) fn route(mut request: Request) -> Result<Response, Error> {
	let s3_credentials = Credentials::load();

	// Only generate authorize header for GET and HEAD request
	// Pass PURGE to backend as it is
	// Block other kinds of method
	let response = match request.get_method() {
		&Method::GET | &Method::HEAD => {
			request = authorize_request(
				request,
				&s3_credentials.access_key_id,
				&s3_credentials.secret_access_key,
				&s3_credentials.region,
				&s3_credentials.host,
			)?;

			let mut response = request.send(BACKEND)?;
			response.remove_header_str("x-amz-id-2");
			response.remove_header_str("x-amz-request-id");
			response.remove_header_str("server");
			response.remove_header_str("x-amz-version-id");
			response.remove_header_str("x-wasabi-cm-reference-id");

			return Ok(response);
		}
		method if method == "PURGE" => {
			// When doing the purge, we need to make sure the cache key matches
			// Cache key is cacualted from URL and HOST header of request sent to backend
			// URL of backend request has no query string
			request.remove_query();
			request.send(BACKEND)?
		}
		_ => Response::from_status(StatusCode::METHOD_NOT_ALLOWED)
			.with_header(header::ALLOW, "GET, HEAD"),
	};

	Ok(response)
}

/// Generate Authorization headers of the S3 request
fn authorize_request(
	mut request: Request,
	access_key_id: &str,
	secret_access_key: &str,
	region: &str,
	host: &str,
) -> Result<Request, Error> {
	// Ignore the query string from client
	request.remove_query();
	let canonical_querystring = "";

	let x_amz_content_sha256 = EMPTY_HASH;
	let amz_date = chrono::Utc::now().format("%Y%m%dT%H%M%SZ").to_string();

	// No need to URL encode twice for S3 object
	// https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
	// "Each path segment must be URI-encoded twice (EXCEPT for Amazon S3 which only gets URI-encoded once)"
	// Do url decode and re-encode in case some clients do not use url encoding according S3 spec.
	// https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-header-based-auth.html
	let path_url_decoded = urlencoding::decode(request.get_path())?;
	let path_url_encoded = urlencoding::encode(&path_url_decoded);
	// No url encode of "/" in object id
	let canonical_uri = path_url_encoded.replace("%2F", "/");

	log::debug!("{}", canonical_uri);

	// Generate Authorization header value
	// Force the method to GET
	let authorization_value = generate_authorization_header(
		access_key_id,
		secret_access_key,
		region,
		host,
		&canonical_uri,
		canonical_querystring,
		x_amz_content_sha256,
		"GET",
		&amz_date,
		"s3",
	);

	request.set_method(Method::GET);

	// Add authorization related headers to request
	request.set_header(header::HOST, host);
	request.set_header(header::AUTHORIZATION, &authorization_value);
	request.set_header("x-amz-content-sha256", x_amz_content_sha256);
	request.set_header("x-amz-date", &amz_date);

	log::debug!(
		"Path: {}, Host: {}, x-amz-date: {}, Authorization: {}",
		request.get_path(),
		host,
		amz_date,
		authorization_value,
	);

	Ok(request)
}
