use fastly::{Error, Request, Response};

pub const BACKEND_ORIGIN: &str = "tech_fillip_pro";

/// Route requests to the home site (on Deno Cloud).
pub fn route(request: Request) -> Result<Response, Error> {
	log_fastly::init_simple("my_log", log::LevelFilter::Info);
	fastly::log::set_panic_endpoint("my_log").unwrap();

	// Send request to backend
	let response = request.send(BACKEND_ORIGIN)?;

	Ok(response)
}