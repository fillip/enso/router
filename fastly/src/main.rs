mod apps;
mod cdn;
mod configuration;
mod home;
mod routes;
mod s3;
mod services;
mod tech;

use chrono::Utc;
use fastly::experimental::{BackendExt, BackendHealth};
use fastly::http::{header, StatusCode};
use fastly::{Backend, Error, Request, Response};
use serde::{Deserialize, Serialize};
use routes::{handle_request, RouteHandler};

const DASH_BACKEND_ORIGIN: &str = "dash_fillip_pro";

/// Entry point to the Fastly Compute process.
///
/// Alternatively the `#[fastly::main]` attribute can be removed, along with the
/// `Request` parameter, and `Response` return value. This would result in a "normal"
/// `main` function,
///
/// To get the `Request` from such an approach means directly using `fastly::Request::from_client()`
/// (which is what the macro is doing underneath). This approach may work in cases where the `main`
/// entrypoint needs to be more versatile.
#[allow(clippy::inline_always)]
#[allow(clippy::unnecessary_wraps)]
#[fastly::main]
fn main(request: Request) -> Result<Response, Error> {
	let log_level = configuration::get_log_level();

	log_fastly::init_simple("enso_logs", log_level);

	let host_header = request.get_header_str(header::HOST).unwrap_or_default();

	match host_header {
		host if host.starts_with("cdn.") => cdn::route(request),
		host if host.starts_with("tech.") => tech::route(request),
		_ => {
			// Get all values for the "Accept" header
			let headers = request.get_header_all_str("Accept");

			// Get the request path and trim any trailing slashes
			let path = request.get_path().trim_end_matches('/').to_owned();

			// Route the request based on the Accept header
			if headers.contains(&"application/json") {
				// Handle service requests (like API requests)
				handle_request(&services::ServiceRoutes::router(), &path, request)
			} else {
				// Handle web app requests (like webpage requests)
				handle_request(&apps::AppsRoutes::router(), &path, request)
			}
		}
	}
}

/// Route (web) application requests to the appropriate backend service. This should be the typical
/// handler for any request that does not include the `Accept: application/json` header key/value.
///
/// For all `Accept: application/json` requests the assumption is made that they are web service requests
/// rather than content requests.
fn route_apps(request: Request) -> Result<Response, Error> {
	match (request.get_path(), request.get_query_str()) {
		// Requests for exactly / => origin 0
		// (query strings not allowed)
		("/", None) => {
			home::route_app(request)
		}
		// Requests for /status and all subpaths => origin 1
		// (query strings allowed)
		(path, _) if path == "/status" || path.starts_with("/status/") => {
			Ok(request.send(DASH_BACKEND_ORIGIN)?)
		}
		// Unrecognised path => 404 error
		_ => Ok(
			Response::from_body("The page you requested could not be found")
				.with_status(StatusCode::NOT_FOUND),
		),
	}
}



/// Route service (non web application) requests.
fn route_services(request: Request) -> Result<Response, Error> {
	match (request.get_path(), request.get_query_str()) {
		// Requests for exactly / => origin 0
		// (query strings not allowed)
		("/", None) => Ok(request.send(home::BACKEND_ORIGIN)?),
		// Requests for /status and all subpaths => origin 1
		// (query strings allowed)
		(path, _) if path == "/status" || path.starts_with("/status/") => {
			route_status(&request)
			//Ok(request.send(DASH_BACKEND_ORIGIN)?)
		}
		// Unrecognised path => 404 error
		_ => Ok(
			Response::from_body("The page you requested could not be found")
				.with_status(StatusCode::NOT_FOUND),
		),
	}
}

// Route requests for the status endpoint.
fn route_status(request: &Request) -> Result<Response, Error> {
	if request.get_path() == "/status" {
		#[derive(Serialize, Deserialize)]
		struct OriginStatus {
			timestamp: String,
			fastly_pop: String,
			home_fillip_pro: bool,
			dash_fillip_pro: bool,
			origin_unhealthy: bool,
		}

		// FASTLY_POP is not available in fiddle
		// Health Check is not available in fiddle
		let origin_staus_json = serde_json::json!({
		  "timestamp": Utc::now().format("%a, %d %h %Y %H:%M:%S UTC").to_string(),
		  "fastly_pop": std::env::var("FASTLY_POP").unwrap_or_else(|_|"TYO".to_string()),
		  "home_fillip_pro": Backend::from_name("home_fillip_pro")?.is_healthy()? == BackendHealth::Healthy,
		  "dash_fillip_pro": Backend::from_name("dash_fillip_pro")?.is_healthy()? == BackendHealth::Healthy,
		  "origin_unhealthy": Backend::from_name("F_origin_unhealthy")?.is_healthy()? == BackendHealth::Healthy,
		});
		Ok(Response::from_body(origin_staus_json.to_string()).with_status(200))
	} else {
		Ok(Response::from_status(StatusCode::NOT_FOUND))
	}
}

#[test]
fn test_homepage() {
	let request = fastly::Request::get("https://fillip.pro/");
	let response = route_apps(request).expect("request succeeds");

	assert_eq!(response.get_status(), StatusCode::NOT_FOUND);
	assert_eq!(
		response.get_content_type(),
		Some(fastly::mime::TEXT_PLAIN_UTF_8)
	); //Some(fastly::mime::TEXT_HTML_UTF_8));
	assert!(response
		.into_body_str()
		.contains("The page you requested could not be found"));
}
