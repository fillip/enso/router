# Enso Router 

[![Deploy to Fastly](https://deploy.edgecompute.app/button)](https://deploy.edgecompute.app/deploy)

The Enso router provides the routing logic for the Enso web projects running at _[Fillip.pro](https://fillip.pro)_. 

## Build

To build the core *Fastly Compute* WebAssembly module you can run the typical *Cargo* command on the terminal:

```shell
cargo build
```

However, this will not result in a version usable on the *Fastly Compute* service as it's lacking the meta data. Instead you will need to have the *Fastly* CLI installed and run:

```shell
fastly compute build
```

Run this command in the `/fastly` sub-directory. The project workspace is currently used as a placeholder structure for eventual expansion into overlapping dependencies and potentially alternative platform options.

The resulting *Fastly Compute* WebAssembly package will be in the `/pkg` sub-directory. 

## Running Locally

To run the router locally you will need the *Fastly* CLI package installed and to run:

```shell
fastly compute serve
```

This command will run the WebAssembly build with the local server settings in the `fastly.toml` file. Please note that service dependencies (such as Deno Cloud) may not route correctly due to the way the origin host is inferred in the requests. 

## Deployment

The infrastructure is built through the [Enso Composer](https://gitlab.com/fillip/enso/composer) program, which automatically provisions all pre-requisites as well as the router itself. If you want to understand how this router fits in with the general setup at [Fastly](https://fastly.com) then check out the *composer* first.

To deploy this router the *Fastly* package needs to be uploaded to the storage location where the *composer* picks it up from. The specifics of that are included in both the [GitLab](https://gitlab.com) CI configuration within this project, and within the *composer* project. 

Simply updating that package alone will not result in an immediate deployment.

## Formatting and linting

```shell
cargo fmt
cargo clippy -- -W clippy::pedantic -D warnings
```